from castle import Castle
from towers import *
from buildings import *
from gameBoard import Board, GameCell
from InGameButton import *
from menubutton import *
from soundEngine import *
import sys


def get_font(size):  # Returns Press-Start-2P in the desired size
    return pygame.font.Font("assets/font.ttf", size)


# class Obstacle:
#     def __init__(self, row, col):
#         self.row = row
#         self.col = col
#         self.width = OBST_WIDTH
#         self.height = OBST_HEIGHT
#         self.img = OBST_ONE_IMG
#         self.x = self.row * GRID_WIDTH
#         self.y = self.col * GRID_HEIGHT

#     def draw(self):
#         Board.draw_img_on_grid(self.img, self.row, self.col)


# ----------------------------------------------------------
class Game:
    # our matrix: 30 column , 23 row
    # gameEntities = [[None]*30]*23


    # window = None
    # obstacles = []
    # gameMatrix=None
    def __init__(self):
        self.window = GAME_WINDOW
        pg.display.set_caption("TOWER DEFENSE")
        self.selected_tower = None
        self.vertical_menu = VerticalMenu(MENU_ITEM_ROW_POS, MENU_ITEM_COL_POS)
        self.running=None
        self.player1 = Castle('Blue', 'player1')
        self.player2 = Castle('Red', 'player2')
        self.SE = SoundEngine()
        self.player1.set_enemy(self.player2)
        self.player2.set_enemy(self.player1)
        self.curr_player = self.player1
        # adding button to buy towers
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 0.5 + self.vertical_menu.y,TOWER_ONE_IMG, 1000, 'tower1'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 2.5 + self.vertical_menu.y,TOWER_TWO_IMG, 2000, 'tower2'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 4.5 + self.vertical_menu.y,TOWER_THREE_IMG,3000, 'tower3'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 6.5 + self.vertical_menu.y,GOLD_MINE_IMG,1000, 'GoldMine'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 8.5 + self.vertical_menu.y,SPELL_HEAL,1000, 'spell_heal'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 10.5 + self.vertical_menu.y,SPELL_DEATH,3000, 'spell_death'))
        self.vertical_menu.add_btn(Button(self.vertical_menu.x + (GRID_HEIGHT * 0.2), GRID_HEIGHT * MENU_ITEM_HEIGHT * 12.5 + self.vertical_menu.y,SPELL_UPGRADE,1500, 'spell_upgrade'))
        # random obstacles
        # Game.player1.towers.append(Tower1(10, 10))
        # randsetX = random.sample(range(0, GRIDHOLDER_H), 4)
        # randsetY = random.sample(range(0, GRIDHOLDER_W), 4)
        # for i, rand in enumerate(randsetX):
        #     while rand < CASTLE_SIZE * 2 and randsetY[i] < CASTLE_SIZE * 2:
        #         rand = random.randint(0, GRIDHOLDER_H)
        #     randsetX[i] = rand
        # for i, rand in enumerate(randsetY):
        #     while rand > (GRIDHOLDER_W - CASTLE_SIZE * 2) and randsetX[i] > CASTLE_SIZE * 2:
        #         rand = random.randint(0, GRIDHOLDER_W)
        #     randsetY[i] = rand

        # [Game.obstacles.append(Obstacle(randsetX[i], randsetY[i])) for i in range(4)]
        self.select_tower = False
        self.toggle_click = False
        self.toggle_soldier_menu = False

        self.run()

    def update_game(self):
        # drawing game background
        self.window.blit(GAME_BACKGROUND_IMG, (0, 0))
        # drawing matrix for visualizing
        # Board.drawMatrix()
        self.SE.draw_button()
        # drawing castles
        self.player1.draw()
        self.player2.draw()
        self.curr_player.draw_castle_info()
        # drawing obstacles
        # for ob in Game.obstacles:
            # ob.draw()
        #drawing remove/update menu
        for tw in self.curr_player.towers:
            if tw.toggle_selected:
                tw.towerMenu.draw()
        # drawing towers


        for tw in self.player1.towers:
            tw.draw()
        for tw in self.player2.towers:
            tw.draw()
        for gm in self.player1.goldMines:
            gm.draw()
        for gm in self.player2.goldMines:
            gm.draw()
        # drawing vertical menu
        self.vertical_menu.draw()

        if self.toggle_soldier_menu:
            self.curr_player.horizontal_menu.draw()
            # drawing dragged tower
        if self.curr_player.movingObject:
            x, y = pg.mouse.get_pos()
            self.curr_player.movingObject.draw_dragged(x, y)
        #drawing fireshots from towers
        for tw in self.player1.towers:
            for sh in tw.shots:
                    sh.draw()
        for tw in self.player2.towers:
            for sh in tw.shots:
                    sh.draw()
          

        pg.display.update()

    def switch_turns(self, currPlayer):
        """switches turns between the two players"""
        # for tw in self.curr_player.towers:
        #     tw.toggle_selected = False
        self.toggle_click = False
        self.toggle_soldier_menu = False

        for tw in self.curr_player.towers:
            if tw.toggle_selected:
                tw.toggle_selected=False
                
        # print('switch turns')

        self.curr_player.timer = 0
        if (currPlayer.name == 'player1'):
            self.curr_player = self.player2
        else:
            self.curr_player = self.player1
        
        self.curr_player.timer = TIMER_TURN
        self.curr_player.movingObject=None

    def check_intersection(self, this_col, this_row, comp_to_col, comp_to_row, fst_Obj_Size, sec_Obj_Size):
        '''
        Function to get if two objects are intersecting or not
        this_col, this_row = The dragged object to be placed
        comp_to_col, comp_to_row = The obj that already exists on the map
        fst_Obj_Size = size of the object to be placed
        sec_Obj_Size = size of the object that already exists on the map

        '''
        R1 = [this_col, this_row, this_col + fst_Obj_Size - 1, this_row + fst_Obj_Size - 1]
        R2 = [comp_to_col, comp_to_row, comp_to_col + sec_Obj_Size - 1, comp_to_row + sec_Obj_Size - 1]

        R3 = [this_col, this_row + fst_Obj_Size - 1, this_col + fst_Obj_Size - 1, this_row]
        R4 = [comp_to_col, comp_to_row + sec_Obj_Size - 1, comp_to_col + sec_Obj_Size - 1, comp_to_row ]


        if   ((R2[0] <= R1[0] and R1[0] <= R2[2]) and (R2[1] <= R1[1] and R1[1] <= R2[3]) ) or ((R2[0] <= R1[2] and R1[2] <= R2[2]) and (R2[1] <= R1[3] and R1[3] <= R2[3]) )or ((R1[0] <= R2[0] and R2[0] <= R1[2]) and (R1[1] <= R2[1] and R2[1] <= R1[3])) or ((R1[0] <= R2[2] and R2[2] <= R1[2]) and (R1[1] <= R2[3] and R2[3] <= R1[3])) :
            return True

        if   ((R4[0] <= R3[0] and R3[0] <= R4[2]) and (R4[1] >= R3[1] and R3[1] >= R4[3]) ) or ((R4[0] <= R3[2] and R3[2] <= R4[2]) and (R4[1] >= R3[3] and R3[3] >= R4[3]) )or ((R3[0] <= R4[0] and R4[0] <= R3[2]) and (R3[1] >= R4[1] and R4[1] >= R3[3])) or ((R3[0] <= R4[2] and R4[2] <= R3[2]) and (R3[1] >= R4[3] and R4[3] >= R3[3])) :
            return True


        return False



    def check_second_in_first_adjacents(self,clicked_col, clicked_row, clicked_obj_size, obj_col, obj_row, obj_size) -> bool:
        """checks if the second (row , col) index is in the adjacent area of (row1,col1) index"""
        # Objects left include(top left and bottom left)
        if ((clicked_col + clicked_obj_size - 1 == obj_col - obj_size + 1 and clicked_row + clicked_obj_size - 1 == obj_row)or(clicked_col + clicked_obj_size - 1 == obj_col - 1 and  clicked_row == obj_row + obj_size ) or (clicked_col + clicked_obj_size - 1 == obj_col - 1 and  clicked_row == obj_row + 1 )or(clicked_col + clicked_obj_size - 1 == obj_col - 1 and clicked_row == obj_row) or (clicked_col + clicked_obj_size - 1 ==  obj_col - 1 and clicked_row + clicked_obj_size - 1 == obj_row - 1)):
            return True
        # Objects Right include(top right and bottom right)
        if ((obj_col + obj_size - 1 == clicked_col - clicked_obj_size + 1 and obj_row + obj_size - 1 == clicked_row) or (obj_col + obj_size - 1 == clicked_col - 1 and  obj_row == clicked_row + clicked_obj_size ) or (obj_col + obj_size - 1 == clicked_col - 1 and  obj_row == clicked_row + 1 )or(obj_col + obj_size - 1 == clicked_col - 1 and obj_row == clicked_row) or (obj_col + obj_size - 1 ==  clicked_col - 1 and obj_row + obj_size - 1 == clicked_row - 1)):
            return True
        # Objects top

        if ((clicked_col + clicked_obj_size - 1 ==  obj_col and clicked_row + clicked_obj_size - 1 == obj_row - obj_size + 1 )or( clicked_col == obj_col and clicked_row + clicked_obj_size - 1 == obj_row - 1) or (clicked_col + clicked_obj_size - 1 == obj_col + obj_size - 1 and clicked_row + clicked_obj_size - 1 ==  obj_row- 1 )or (clicked_col == obj_col + obj_size - 1 and clicked_row + clicked_obj_size - 1 == obj_row - obj_size + 1)):
            return True
        # Objects bottom
        if ((obj_col + obj_size - 1 ==  clicked_col and obj_row + obj_size - 1 == clicked_row - clicked_obj_size + 1 )or( obj_col == clicked_col and obj_row + obj_size - 1 == clicked_row - 1) or (obj_col + obj_size - 1 == clicked_col + clicked_obj_size - 1 and obj_row + obj_size - 1 ==  clicked_row- 1 )or (obj_col == clicked_col + clicked_obj_size - 1 and obj_row + obj_size - 1 == clicked_row - clicked_obj_size + 1)):
            return True
        return False

    def build_around_castle(self, curr_player, clicked_obj_col, clicked_obj_row, castle_col, castle_row):
        if curr_player == "player1":
            for i in range(CASTLE_SIZE + 1):
                if(clicked_obj_col == castle_col + CASTLE_SIZE and clicked_obj_row == i ):
                    return True
                if(clicked_obj_col == i and clicked_obj_row == castle_row + CASTLE_SIZE):
                    return True
        if curr_player == "player2":
            for i in range(CASTLE_SIZE + 2):
                if(clicked_obj_col + TOWER_SIZE - 1 == castle_col - 1 and clicked_obj_row + TOWER_SIZE - 1  == i + castle_row - 1):
                    return True
                if(clicked_obj_col + TOWER_SIZE - 1 == i + castle_col - 1 and clicked_obj_row + TOWER_SIZE - 1  == castle_row - 1):
                    return True
        return False
    def check_other_side_of_path(self, col, row):
        col_right = col + TOWER_SIZE - 1
        row_bottom = row + TOWER_SIZE - 1
        #Checking the right side
        # if(Board.gameMatrix[col_right + 1][row - 1] == 1  or Board.gameMatrix[col_right + 1][row] == 1  or  Board.gameMatrix[col_right + 1][row_bottom] == 1  or Board.gameMatrix[col_right + 1][row_bottom + 1] == 1 ):
        for tw in self.curr_player.towers + self.curr_player.goldMines:
            if( self.check_second_in_first_adjacents(col_right + 1, row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                self.check_second_in_first_adjacents(col_right + 1, row, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                    self.check_second_in_first_adjacents(col_right + 1, row_bottom, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                    self.check_second_in_first_adjacents(col_right + 1, row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) ):
                    return True
            
            if( self.check_second_in_first_adjacents(col - 1, row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                    self.check_second_in_first_adjacents(col - 1, row, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col - 1, row_bottom, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col - 1, row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) ):
                     return True

            if( self.check_second_in_first_adjacents(col - 1, row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                    self.check_second_in_first_adjacents(col , row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col_right, row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col_right + 1, row - 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) ):
                     return True
            
            if( self.check_second_in_first_adjacents(col - 1, row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                    self.check_second_in_first_adjacents(col , row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col_right, row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) or
                     self.check_second_in_first_adjacents(col_right + 1, row_bottom + 1, TOWER_SIZE, tw.col, tw.row, TOWER_SIZE) ):
                     return True

        return False

    def can_place(self, row, col) -> bool:
        # return True
        """checks if we can place tower or gold mine in a give place (row,col), and buys it"""
        # print("can place tower/goldmine is called")
        # check  building on top of an obstacle

        # check  building on top of your own towers
        for tw in self.curr_player.towers:
            if self.check_intersection(col, row, tw.col, tw.row, TOWER_SIZE, TOWER_SIZE):
                # print("Cant build because building on top of your own towers")
                return False
        # check  building on top of your own castle
        if self.check_intersection(col, row, self.curr_player.col ,self.curr_player.row , TOWER_SIZE, CASTLE_SIZE):
            # print("Cant build because building on top of your own castle")
            return False
        # check if not building on gold mine
        for gm in self.curr_player.goldMines:
            if self.check_intersection(col, row, gm.col, gm.row, TOWER_SIZE, TOWER_SIZE):
                # print("Cant build because building on top of your own gold mine")
                return False
        # check if we are building int enemy vicinity
        if self.curr_player.name == 'player1':
            if self.check_intersection(col, row, self.player2.col ,self.player2.row , TOWER_SIZE, CASTLE_SIZE):
                # print("Cant build because building on top of your enemy castle")
                return False
            for tw in self.player2.towers:
                if self.check_intersection(col, row, tw.col, tw.row, TOWER_SIZE, TOWER_SIZE):
                    # print("Cant build because building on top of your enemy towers")
                    return False
            for gm in self.player2.goldMines:
                if self.check_intersection(col, row, gm.col, gm.row, TOWER_SIZE, TOWER_SIZE):
                    print("Cant build because building on top of your enemy gold mines")
                    return False
            # for tw in self.player2.towers:
            #     if self.check_second_in_first_adjacents(row, col, tw.row, tw.col):
            #         return False
        elif self.curr_player.name == 'player2':
            if self.check_intersection(col, row, self.player1.col ,self.player1.row , TOWER_SIZE, CASTLE_SIZE):
                print("Cant build because building on top of your enemy castle")
                return False
            for tw in self.player1.towers:
                if self.check_intersection(col, row, tw.col, tw.row, TOWER_SIZE, TOWER_SIZE):
                    print("Cant build because building on top of your enemy towers")
                    return False
            for gm in self.player1.goldMines:
                if self.check_intersection(col, row, gm.col, gm.row, TOWER_SIZE, TOWER_SIZE):
                    print("Cant build because building on top of your enemy gold mine")
                    return False

        print( col + TOWER_SIZE - 1, len(Board.gameMatrix[col]) )
        if row >= 0 and col >= 0 and row + TOWER_SIZE - 1 < len(Board.gameMatrix[row]) and col + TOWER_SIZE - 1 < len(Board.gameMatrix[col]) :
            if Board.gameMatrix[row][col + TOWER_SIZE - 1] > 0 or Board.gameMatrix[row + TOWER_SIZE - 1][col + TOWER_SIZE - 1] > 0 or  Board.gameMatrix[row + TOWER_SIZE - 1][col ] > 0 or Board.gameMatrix[row + TOWER_SIZE - 1][col]  > 0:
                return False
        else:
            return False

        # check  building in  your surrounding area:
        # building next to castle
        # if self.check_second_in_first_adjacents(col, row,TOWER_SIZE, self.curr_player.row,self.curr_player.col , CASTLE_SIZE):
        #     return True
        # # building next to towers
        for tw in self.curr_player.towers:
            if self.check_second_in_first_adjacents(col, row,TOWER_SIZE, tw.col, tw.row, TOWER_SIZE):
                return True

        if self.build_around_castle(self.curr_player.name ,col, row,self.curr_player.col ,self.curr_player.row ):
            return True


        if self.check_other_side_of_path(col, row):
            return True

        return False


    # math.floor(pos[1]/GRID_WIDTH)->rows
    # math.floor(pos[0]//GRIDH_HEIGHT->cols
    def run(self):
        self.SE.play_background_music("mario_kart.wav")

        self.SE.mute_unmute_music()
        self.running = True
        clock = pg.time.Clock()
        towerclicked = False


        while self.running:

            clock.tick(FPS)
            pos = pg.mouse.get_pos()

            # getting (row,col) index based grid from given (x,y) coordinates
            row = math.floor(pos[1] / GRID_WIDTH)
            col = math.floor(pos[0] / GRID_HEIGHT)
            # if the menu item is dragged and we already have chosen a menu item
            if self.curr_player.movingObject:
                self.curr_player.movingObject.move(row, col)

            for event in pg.event.get():
                if event.type == pg.QUIT:
                    self.running = False
                # switching turns based on timer
                if event.type == pg.USEREVENT:
                    if (self.curr_player.timer % 2 == 0):
                        self.curr_player.increase_gold()
                    self.curr_player.timer -= 1
                    # if self.curr_player.timer > 0:
                        # print(self.curr_player.timer)
                    if (self.curr_player.timer <= 0):
                        self.switch_turns(self.curr_player)
                    #drawing fireshots from towers after some time
                   

                # ----------- Checking for soldier menu
                if (event.type == pg.MOUSEBUTTONDOWN):
                    #check for sound button
                    if self.SE.click(pos[0],pos[1]):
                        self.SE.mute_unmute_music()
                    for tw in self.curr_player.towers:
                        clicked = tw.click(math.floor(
                            pos[0] / GRID_HEIGHT), math.floor(pos[1] / GRID_WIDTH))
                        tw.towerMenu.buttons[0].price = int(tw.upgradeCost)
                        tw.towerMenu.buttons[1].price = int(tw.upgradeCost * 0.4)
                        if clicked and not tw.toggle_selected:
                            tw.toggle_selected=True

                        if not clicked and tw.toggle_selected:
                            tw.toggle_selected=False
                            # checking if we wanna update or remove from towers menu
                            for btn in tw.towerMenu.buttons:
                                item_name = btn.click(pos[0], pos[1])
                                # print(item_name)
                                if item_name != None:
                                    # print(len(self.curr_player.towers))
                                    if item_name == "update":
                                        if self.curr_player.gold >= tw.upgradeCost:
                                            self.curr_player.gold -= tw.upgradeCost
                                            tw.upgrade()
                                    # removing tower and getting 40 % of it's cost
                                    elif item_name == "remove":
                                        self.curr_player.towers.remove(tw)
                                        self.curr_player.gold += tw.buyCost*0.4
                        # print('you clicked away from a tower')
                        # updating upgrading and selling cost
                    #checking if we bought sth from vertical menu

                    if self.curr_player.click(pos[0], pos[1]) != None and not self.toggle_click:
                        self.toggle_click = True
                        self.toggle_soldier_menu = True
                        towerclicked = True

           
                    elif self.curr_player.click(pos[0], pos[1]) != None and self.toggle_click:
                        self.toggle_click = False
                        self.toggle_soldier_menu = False
      
                    else:
                        for btn in self.curr_player.horizontal_menu.buttons:
                            item_name = btn.click(pos[0], pos[1])
                            if (item_name != None) and towerclicked:
                                self.curr_player.add_soldier(item_name)
                                break
                        towerclicked = False
                        self.toggle_click = False
                        self.toggle_soldier_menu = False

                    # print(row,col)

                    # if we have selected an item fom the menu and it's moving
                    if self.curr_player.movingObject:
                        # for now it handels only building towers
                        if(self.can_place(row,col)):
                            self.curr_player.movingObject.row = row
                            self.curr_player.movingObject.col = col
                            self.curr_player.movingObject.x = col * GRID_WIDTH
                            self.curr_player.movingObject.y = row * GRID_HEIGHT
                            if (isinstance(self.curr_player.movingObject, GoldMine)):

                                self.curr_player.gold -= self.curr_player.movingObject.buyCost
                                self.curr_player.goldMines.append(self.curr_player.movingObject)
                                print(self.curr_player.movingObject.x,self.curr_player.movingObject.y)
                            elif (isinstance(self.curr_player.movingObject, Tower)):
      
                                self.curr_player.gold -= self.curr_player.movingObject.buyCost
                                self.curr_player.towers.append(self.curr_player.movingObject)

                        else:
                            print('you cant build here')
                        self.curr_player.movingObject = None

                    else:
                        # selecting an item from the vertical menu
                        for btn in self.vertical_menu.buttons:
                            item_name = btn.click(pos[0], pos[1])
                            if (item_name != None):
                                if item_name.find('spell') != -1:
                                    self.curr_player.activateSpell(item_name)
                                else:
                                    self.curr_player.add_tower_or_building(item_name, row, col)

            for tw in self.player1.towers:
                tw.attack(self.player2.soldiers)
                #remove shots
                for sh in tw.shots:
                    if sh.exploaded:
                        tw.shots.remove(sh)
            for tw in self.player2.towers:
                tw.attack(self.player1.soldiers)
                #remove shots
                for sh in tw.shots:
                    if sh.exploaded:
                        tw.shots.remove(sh)
            # Game.player2.health=0
            winner=""
            if self.player1.destroyed():
                winner="player2"
            elif self.player2.destroyed():
                winner="player1"
            if winner!="":
                self.running=False
                main_menu(winner)
            self.update_game()

            clockobject = pg.time.Clock()
            clockobject.tick(20)
        pg.quit()


def main_menu(winner=""):
    """ main_menu or a win_screen"""
    pygame.init()
    SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

    pygame.display.set_caption("Menu")
    backGround =  pg.transform.scale(pygame.image.load(os.path.join('Assets','Background.png')), (SCREEN_WIDTH, SCREEN_HEIGHT))
    running = True

    while running:
        SCREEN.blit(backGround, (0, 0))

        MENU_MOUSE_POS = pygame.mouse.get_pos()

        MENU_TEXT = get_font(70).render(winner+" Won" if(winner!="") else "Tower Defense", True, "#b68f40")
        MENU_RECT = MENU_TEXT.get_rect(center=(SCREEN_WIDTH-SCREEN_WIDTH//2, SCREEN_HEIGHT-SCREEN_HEIGHT//(2) if (winner!="") else (SCREEN_HEIGHT-SCREEN_HEIGHT//1.5) ))

        button_list = []

        PLAY_BUTTON = MenuButton(image=None, pos=(SCREEN_WIDTH-SCREEN_WIDTH//2,650 if(winner!="") else 450),
                                 text_input="PLAY AGAIN" if(winner!="") else "PLAY", font=get_font(30 if(winner!="") else 60), base_color="Black", hovering_color="White")
        button_list.append(PLAY_BUTTON)
        QUIT_BUTTON = MenuButton(image=None, pos=(SCREEN_WIDTH-SCREEN_WIDTH//2,850 if(winner!="") else 650),
                                 text_input="QUIT", font=get_font(30 if (winner!="") else 60), base_color="Black", hovering_color="White")
        button_list.append(QUIT_BUTTON)

        SCREEN.blit(MENU_TEXT, MENU_RECT)

        for button in button_list:
            button.changeColor(MENU_MOUSE_POS)
            button.update(SCREEN)
        temp = 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                running = False
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if PLAY_BUTTON.checkForInput(MENU_MOUSE_POS):
                    running = False
                    Game()
                    temp = 1
                if QUIT_BUTTON.checkForInput(MENU_MOUSE_POS):
                    pygame.quit()
                    running = False
                    sys.exit()

        if temp == 0:
            pygame.display.update()
