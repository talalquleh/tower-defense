from operator import truediv
import soldiers
from towers import *
from buildings import GoldMine
from menus import HorizontalMenu, Button
from soldiers import *


class Castle:
    def __init__(self, color: str, name: str):
        self.row = 0
        self.col = 0
        self.width = CASTLE_WIDTH
        self.height = CASTLE_HEIGHT
        self.health = 1000
        self.gold = 40000
        self.soldiers = []
        self.towers = []
        self.goldMines = []
        self.spells = []
        self.won = False
        self.color = color
        self.name = name
        self.window = GAME_WINDOW
        self.movingObject = None
        self.timer = TIMER_TURN
        self.enemy = None
        if (color == 'Red'):
            """choosing the player's bar according to color """
            self.row, self.col = RED_CASTLE_POS[RED_RAND_POS]
            self.img = RED_CASTLE_IMG
            # self.goldMine = GoldMine(6, random.randint(15, 23))
        else:
            self.row, self.col = BLUE_CASTLE_POS[BLUE_RAND_POS]
            self.img = BLUE_CASTLE_IMG

        self.x = self.col * GRID_WIDTH
        self.y = self.row * GRID_HEIGHT
        # ---------- Soldier Menu
        if self.color == 'Blue':
            self.horizontal_menu = HorizontalMenu((self.col + MENU_ITEM_WIDTH / 2) * GRID_HEIGHT,
                                                  (self.row + self.height / GRID_WIDTH) * GRID_WIDTH)
        else:
            self.horizontal_menu = HorizontalMenu((self.col - 5) * GRID_HEIGHT, (self.row - 1) * GRID_WIDTH)

        self.horizontal_menu.add_btn(
            Button(GRID_HEIGHT * 0 + self.horizontal_menu.x, self.horizontal_menu.y, SOLDIER_1, 1000, 'Soldier1'))
        self.horizontal_menu.add_btn(
            Button(GRID_HEIGHT * 3 + self.horizontal_menu.x, self.horizontal_menu.y, SOLDIER_2, 2000, 'Soldier2'))
        self.horizontal_menu.add_btn(
            Button(GRID_HEIGHT * 6 + self.horizontal_menu.x, self.horizontal_menu.y, SOLDIER_3, 3000, 'Soldier3'))

    def set_enemy(self, enemy):
        self.enemy = enemy

    def draw(self):
        Board.draw_img_on_grid(self.img, self.row, self.col)
        # self.goldMine.draw()
        for sold in self.soldiers:
            sold.draw()
        # self.window.blit(
        # self.img, (self.x-self.width//2, self.y-self.height//2))

    def draw_castle_info(self):
        """draws player's name ,how much money, health, and time he has"""
        if (self.color == 'Red'):
            self.window.blit(RED_PLAYER_BAR_IMG, ((SCREEN_WIDTH * 2.5 / 4) - BAR_WIDTH, -15))
        else:
            self.window.blit(BLUE_PLAYER_BAR_IMG, ((SCREEN_WIDTH * 2.5 / 4) - BAR_WIDTH, -15))

        player_info = MENU_FONT.render(str(self.name) + " Turn- " +
                                       str(self.health) + " HP - " + str(self.gold) + " $- " + str(self.timer) + "sec",
                                       True, BLACK)
        self.window.blit(
            player_info, (((SCREEN_WIDTH * 2.5 / 4) - BAR_WIDTH) + 50, BAR_HEIGHT // 4))

    def add_tower_or_building(self, name: str, row, col) -> bool:
        """adds a tower to player's towers in a give (row,col) indx ,
           helping us to track selected towers from the menu
           and return whither we can buy the tower or not"""
        obj = None
        if name == 'tower1':
            obj = Tower1(row, col)
            if self.color == 'Red':
                obj.img = RED_TOWER_ONE_IMG
            else:
                obj.img = BLUE_TOWER_ONE_IMG
        elif name == 'tower2':
            obj = Tower2(row, col)
            if self.color == 'Red':
                obj.img = RED_TOWER_TWO_IMG
            else:
                obj.img = BLUE_TOWER_TWO_IMG
        elif name == 'tower3':
            obj = Tower3(row, col)
            if self.color == 'Red':
                obj.img = RED_TOWER_THREE_IMG
            else:
                obj.img = BLUE_TOWER_THREE_IMG
        elif name == 'GoldMine':
            obj = GoldMine(row, col)
            obj.img = GOLD_MINE_IMG
        # we buy the item
        if obj.buyCost <= self.gold:
            self.movingObject = obj
            return True
        # print('not enough money')
        return False

    def soldier_killed(self, name):
        for i, sold in enumerate(self.soldiers):
            if sold.name == name:
                del self.soldiers[i]
                break

    def bulidings_killed(self, name):

        for i, tow in enumerate(self.towers):
            if tow == name:
                del self.towers[i]
                break
        for i, tow in enumerate(self.goldMines):
            if tow == name:
                del self.goldMines[i]
                break

    def add_soldier(self, name):
        obj = None
        soldName = 'FootMan ' + str(len(self.soldiers) + 1)
        if name == 'Soldier1':
            obj = Soldier1(soldName, self.row, self.col, self, self.enemy)
            obj.load_imgs(ANIMATED_SOLDIER1)
        if name == 'Soldier2':
            obj = Soldier2(soldName, self.row, self.col, self, self.enemy)
            obj.load_imgs(ANIMATED_SOLDIER2)
        if name == 'Soldier3':
            obj = Soldier3(soldName, self.row, self.col, self, self.enemy)
            obj.load_imgs(ANIMATED_SOLDIER3)

        if obj.buyCost <= self.gold:
            self.gold -= obj.buyCost
            self.soldiers.append(obj)
            return True
        # print('not enough money')
        return False

    def get_hit(self, damage):
        if self.timer % 2 == 1:
            if self.health > 0:
                self.health -= damage

    def increase_gold(self):
        for gm in self.goldMines:
            self.gold += gm.incGold()

    def click(self, X, Y) -> str:
        """returns the name of the button we have clicked"""
        if X <= self.x + self.width and X >= self.x:

            if Y <= self.y + self.height and Y >= self.y:
                # print(self.name)
                return self.name

    def destroyed(self):
        return self.health <= 0

    def activateSpell(self, name):
        if name == 'spell_heal' and self.gold >= 1000:
            something_happened = False

            for soldier in self.soldiers:
                soldier.health += 100
                something_happened = True

            if something_happened:
                self.gold -= 1000

        elif name == 'spell_death' and self.gold >= 3000:
            something_happened = False
            length = len(self.enemy.soldiers)
            ind = 0
            while ind < length:
                if (self.enemy.soldiers[ind].takeDmg(100)):
                    self.enemy.soldiers[ind].die()
                    something_happened = True
                    ind -= 1
                    length -= 1
                ind += 1

            if something_happened:
                self.gold -= 3000

        elif name == 'spell_upgrade' and self.gold >= 1500:
            something_happened = False
            name = ""
            for soldier in self.soldiers:
                if soldier.type == 'Soldier1':
                    name = soldier.name
                    soldier.die()
                    something_happened = True
                    break

            if something_happened:
                self.add_soldier('Soldier3')
                self.soldiers[-1].name = name

            if something_happened:
                self.gold -= 1500






