import sys
sys.path.append("../codedrivers")

from main import *
from soldiers import *


def test_castle():
    c = Castle("Blue", "Castle1")
    assert c.gold == 40000
    assert c.health == 1000
    assert c.width == CASTLE_WIDTH
    assert c.height == CASTLE_HEIGHT
    assert c.soldiers == []
    assert c.towers == []
    assert c.spells == []
    assert c.won == False  
    assert c.name == "Castle1"
    assert c.color == "Blue"
    assert not c.won 


def test_castlefucs():
    c = Castle("Blue", "Castle1")
    assert c.destroyed() == False
    c.set_enemy(None)
    assert c.enemy == None


def test_tower():
    c = Tower2(5, 5)
    assert c.row == 5
    assert c.col == 5
    assert not c.inRange
    assert not c.toggle_selected 
    assert not c.moving
    assert c.name == ''
    assert c.x == c.row * GRID_WIDTH
    assert c.y == c.col * GRID_HEIGHT
    assert c.width == TOWER_WIDTH
    assert c.height == TOWER_HEIGHT


def test_tower1():
    c = Tower1(5, 5)
    assert c.buyCost == 1000
    assert c.reloadTime == 5
    # assert c.range_cells == TOWER_SIZE+2
    assert c.timer == 0
    assert c.range == math.floor(c.range_cells / 2) - 1
    assert c.fire_img == ROCK_TOWER_IMG
    assert c.damage == 15


def test_tower2():
    c = Tower2(5, 5)
    assert c.buyCost == 2000
    assert c.reloadTime == 5
    # assert c.range_cells == TOWER_SIZE+4
    assert c.timer == 0
    assert c.range == math.floor(c.range_cells / 2) - 1
    assert c.fire_img == ROCK_TOWER_IMG
    assert c.damage == 20


def test_tower3():
    c = Tower3(5, 5)
    assert c.buyCost == 3000
    assert c.reloadTime == 6
    # assert c.range_cells == TOWER_SIZE+6
    assert c.timer == 0
    assert c.range == math.floor(c.range_cells / 2) - 1
    assert c.fire_img == ROCK_TOWER_IMG
    assert c.damage == 25


def test_soldier():
    c = Soldier("Troop1",10, 10, Castle("Blue", "Castle1"),Castle("Red", "Castle2"), SOLDIER_1)
    assert c.name == "Troop1"
    assert c.animationCnt == 0
    assert c.owner.name == "Castle1"
    assert c.enemy.name == "Castle2"
    assert c.window ==GAME_WINDOW


def test_soldierfunc():
    c = Soldier1("Troop1",10, 10, Castle("Blue", "Castle1"),Castle("Red", "Castle2"))
    assert c.alive() == True
    assert c.takeDmg(20) == False
    assert c.health == 80


def test_soldier1():
    s = Soldier1("Troop1",10, 10, Castle("Blue", "Castle1"),Castle("Red", "Castle2"))
    assert s.buyCost == 1000
    assert s.health == SOLDIER_1_HEALTH
    assert s.type == 'Soldier1'
    assert s.atcPwer == 1


def test_soldier2():
    s = Soldier2("Troop2",10, 10, Castle("Blue", "Castle1"),Castle("Red", "Castle2"))
    assert s.buyCost == 2000
    assert s.health == SOLDIER_2_HEALTH
    assert s.type == 'Soldier2'
    assert s.atcPwer == 2


def test_soldier3():
    s = Soldier3("Troop3",10, 10, Castle("Blue", "Castle1"),Castle("Red", "Castle2"))
    assert s.health == SOLDIER_3_HEALTH
    assert s.buyCost == 3000
    assert s.type == 'Soldier3'
    assert s.atcPwer == 3


def test_buildings():
    c = GoldMine(15,15)
    assert c.row == 15
    assert c.col == 15
    assert c.x == c.row * GRID_WIDTH
    assert c.y == c.col * GRID_HEIGHT
    assert c.width == GRID_WIDTH
    assert c.height == GRID_HEIGHT
    assert c.img == GOLD_MINE_IMG


def test_buildingsfucs():
    c = GoldMine(15,15)
    c.get_hit(300)
    assert c.health == 200
    c.move(12,12)
    assert c.row == 12
    assert c.col == 12


def test_menu():
    c = Menu(50, 50, 25, 25, VERTICAL_MENU)
    assert c.x == 50
    assert c.y == 50
    assert c.width == 25
    assert c.height == 25

#def test_menufuncs():
    #g = Game()
#    assert g.check_second_in_first_adjacents( 2, 4, 2, 4) == True 
#    assert g.check_second_in_first_adjacents( 1, 2, 3, 4) == True 
#    assert g.check_second_in_first_adjacents( 4, 3, 2, 1) == True 
#    assert g.check_second_in_first_adjacents( 4, 4, 4, 4) == True 


# def test_obstacle():
#     o = Obstacle(10,10)
#     assert o.row == 10
#     assert o.col == 10


def test_gamecell():
    gc = GameCell()
    assert gc.parent == None
    assert gc.position == None
    assert gc.g == 0
    assert gc.h == 0
    assert gc.f == 0

def test_pausebutton():
    #pb = PauseButton(None, (10,10), 1, 1)
    assert True == True
	#assert pb.rect.x == 1
	#assert pb.rect.y == 1
	#assert pb.clicked == False
    

def test_board():
    b = Board()
    assert b.gameMatrix == [
        [0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
        [2,2,0,2,2,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [2,2,1,2,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,0,0,0,0,1,2,2,2,2,0,0,0,1,0,0,0,2,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,2,2,2,2,1,2,2,2,2,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,2,2,2,2,1,2,2,2,2,0,0,0,1,1,1,1,1,1,1,1,0,0],
        [0,0,1,0,0,0,0,0,2,2,2,2,1,2,2,2,2,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,2,2,2,2,1,0,0,0,0,0,0,0,1,2,2,2,2,2,0,1,0,0],
        [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,1,0,0],
        [0,0,1,0,0,2,0,0,2,0,0,2,1,0,0,0,0,0,0,0,1,2,2,2,2,0,0,1,0,0],
        [0,0,1,0,0,2,2,2,2,0,0,0,1,0,2,2,2,2,0,0,1,2,2,2,2,0,0,1,0,0],
        [0,0,1,0,2,2,2,2,2,2,0,0,1,0,2,2,2,2,0,0,1,2,0,0,0,0,0,1,0,0],
        [0,0,1,0,0,2,2,2,2,0,0,0,1,0,2,2,2,2,0,0,1,1,1,1,1,1,1,1,0,0],
        [0,0,1,0,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,2,2,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,1,0,0,2,2,0,0,1,0,0],
        [0,0,1,0,0,2,2,0,0,1,0,0,2,0,0,2,2,0,0,0,1,0,2,2,2,2,0,1,0,0],
        [0,0,1,0,0,2,2,0,0,1,0,0,2,0,0,2,2,0,0,0,1,0,2,2,2,2,0,1,0,0],
        [0,0,1,0,0,2,2,0,0,1,2,0,0,0,0,2,2,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,0,0,0,0,0,1,2,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0],
        [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0],
        [0,0,1,0,0,0,0,0,0,0,0,0,0,1,2,2,2,2,0,0,0,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,2,2,2,2,0,0,0,0,0,1,2,2,2,2,0,0,0,0,0,0,0,0,0,1,0,0],
        [0,0,1,0,2,2,2,2,0,0,0,0,0,1,2,2,2,2,0,0,0,0,0,0,0,0,2,1,2,2],
        [0,0,1,0,0,0,0,0,0,0,0,0,0,1,2,2,2,2,0,0,0,0,0,0,0,2,2,0,2,2],
        [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0]]

    
def test_gamevars():
    assert SCREEN_WIDTH == 960
    assert SCREEN_HEIGHT == 960
    assert GRID_WIDTH == 32 
    assert GRID_HEIGHT == 32

    assert CASTLE_SIZE == 5
    assert TOWER_WIDTH == TOWER_SIZE*GRID_HEIGHT
    assert TOWER_HEIGHT == TOWER_SIZE*GRID_HEIGHT
    assert CASTLE_WIDTH == GRID_HEIGHT*CASTLE_SIZE
    assert CASTLE_HEIGHT ==  GRID_HEIGHT*CASTLE_SIZE

    assert MENU_ITEM_WIDTH ==  1.5
    assert MENU_ITEM_HEIGHT == 1.5  

    assert MENU_ITEM_ROW_POS ==  SCREEN_WIDTH - ((MENU_ITEM_WIDTH+0.3) * GRID_WIDTH)
    assert MENU_ITEM_COL_POS == (SCREEN_HEIGHT // 12)
    assert H_MENU_ITEM_ROW_POS == 0
    assert H_MENU_ITEM_COL_POS == 0


    assert BLUE_CASTLE_POS == [(0,0), (0,0)]
    assert ROWS == SCREEN_HEIGHT // GRID_HEIGHT
    assert COLS ==  SCREEN_WIDTH // GRID_WIDTH
    assert SOLDIER_SIZE == 1


    assert OBST_SIZE == 1.8
    assert BAR_WIDTH == 300
    assert BAR_HEIGHT == 90
    assert BUTTON_WIDTH == 100
    assert BUTTON_HEIGHT ==  40

    assert RED == (255, 0, 0)
    assert BLACK == (0, 0, 0)
    assert WHITE == (255, 255, 255)
    assert VELOCITY == 5
    assert FPS == 60
    assert DIFF_TO_CENTER == 1.2
    # assert TIMER_TURN == 10
    assert GREEN == (0, 255, 0)
    assert RED == (255, 0, 0)
    assert YELLOW == (255, 255, 0)
    assert SOLDIER_1_HEALTH == 100
    assert SOLDIER_2_HEALTH == 100
    assert SOLDIER_3_HEALTH == 100

def test_music_btn():
    se = SoundEngine()
    assert se.x == SCREEN_WIDTH-se.width
    assert se.y == 15
    assert se.width==30
    assert se.height==30

def test_adding_towers_player1():
    p1=Castle('Red','player1')
    assert len(p1.towers)==0
    t = 'tower1'
    p1.add_tower_or_building(t,1,1)
    p1.towers.append(p1.movingObject)
    assert  len(p1.towers)==1
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    p1.add_tower_or_building(t2,1,1)
    p1.towers.append(p1.movingObject)
    p1.add_tower_or_building(t3,1,1)
    p1.towers.append(p1.movingObject)
    p1.add_tower_or_building(t4,1,1)
    p1.towers.append(p1.movingObject)
    assert len(p1.towers)==4
    t5 = 'tower1'
    p1.add_tower_or_building(t5,1,1)
    p1.towers.append(p1.movingObject)
    assert len(p1.towers)==5


def test_adding_towers_player2():
    p2=Castle('Blue','player2')
    assert len(p2.towers)==0
    t = 'tower1'
    p2.add_tower_or_building(t,1,1)
    p2.towers.append(p2.movingObject)
    assert  len(p2.towers)==1
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    p2.add_tower_or_building(t2,1,1)
    p2.towers.append(p2.movingObject)
    p2.add_tower_or_building(t3,1,1)
    p2.towers.append(p2.movingObject)
    p2.add_tower_or_building(t4,1,1)
    p2.towers.append(p2.movingObject)
    assert len(p2.towers)==4
    t5 ='tower1'
    p2.add_tower_or_building(t5,1,1)
    p2.towers.append(p2.movingObject)
    assert len(p2.towers)==5


def test_adding_gold_mines_player1():
    p = Castle('Red','player1')
    assert len(p.goldMines)==0
    g1='GoldMine'
    p.add_tower_or_building(g1,1,1)
    p.goldMines.append(p.movingObject)

    assert len(p.goldMines)==1
    g2='GoldMine'
    p.add_tower_or_building(g2,1,1)
    p.goldMines.append(p.movingObject)
    g3='GoldMine'
    p.add_tower_or_building(g3,1,1)
    p.goldMines.append(p.movingObject)
    g4='GoldMine'
    p.add_tower_or_building(g4,1,1)
    p.goldMines.append(p.movingObject)
    assert len(p.goldMines)==4


def test_adding_gold_mines_player2():
    p = Castle('Red','player2')
    assert len(p.goldMines)==0
    g1='GoldMine'
    p.add_tower_or_building(g1,1,1)
    p.goldMines.append(p.movingObject)
    assert len(p.goldMines)==1
    g2='GoldMine'
    p.add_tower_or_building(g2,1,1)
    p.goldMines.append(p.movingObject)
    g3='GoldMine'
    p.add_tower_or_building(g3,1,1)
    p.goldMines.append(p.movingObject)
    g4='GoldMine'
    p.add_tower_or_building(g4,1,1)
    p.goldMines.append(p.movingObject)
    assert len(p.goldMines)==4

def test_adding_soldiers_player1():
    p = Castle('Red','player1')
    p_e = Castle('Blue','player2')
    assert len(p.soldiers)==0
    s1='Soldier1'
    p.add_soldier(s1)
    assert len(p.soldiers)==1
    s2='Soldier1'
    p.add_soldier(s2)
    assert len(p.soldiers)==2
    s3='Soldier1'
    p.add_soldier(s3)
    assert len(p.soldiers)==3

def test_adding_soldiers_player2():
    p = Castle('Blue','player2')
    p_e = Castle('Red','player1')
    assert len(p.soldiers)==0
    s1='Soldier1'
    p.add_soldier(s1)
    assert len(p.soldiers)==1
    s2='Soldier1'
    p.add_soldier(s2)
    assert len(p.soldiers)==2
    s3='Soldier1'
    p.add_soldier(s3)
    assert len(p.soldiers)==3

def test_gold_mine_func():
    p = Castle('Blue','player2')
    assert p.gold == 40000
    g1='GoldMine'
    p.add_tower_or_building(g1,1,1)
    p.goldMines.append(p.movingObject)
    p.increase_gold()
    assert p.gold>40000




def test_winning_func():
    p = Castle('Blue','player2')
    p_e = Castle('Red','player1')
    p.health=0
    assert p.destroyed()
    assert not  p_e.destroyed()
    p.health=10
    p_e.health=0
    assert p_e.destroyed()
    assert not  p.destroyed()



def test_removing_towers_func():
    p = Castle('Blue','player2')
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    p.add_tower_or_building(t2,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t3,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t4,1,1)
    p.towers.append(p.movingObject)
    assert len(p.towers)==3


def test_updating_towers_func():
    p = Castle('Blue','player2')
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    p.add_tower_or_building(t2,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t3,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t4,1,1)
    p.towers.append(p.movingObject)
    assert p.towers[0].level==1
    p.towers[0].upgrade()
    assert p.towers[0].level==2
    p.towers[0].upgrade()
    assert p.towers[0].level==3
    p.towers[0].upgrade()
    assert p.towers[0].level==3
    assert p.towers[1].level==1



def test_buying_cost_adding():
    p = Castle('Blue','player2')
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    assert p.gold == 40000
    p.add_tower_or_building(t2,1,1)
    p.towers.append(p.movingObject)
    p.gold -= p.movingObject.buyCost
    p.add_tower_or_building(t3,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t4,1,1)
    p.towers.append(p.movingObject)
    assert p.gold<40000

def test_buying_cost_updating():
    p = Castle('Blue','player2')
    t2 = 'tower1'
    t3 = 'tower1'
    t4 = 'tower1'
    assert p.gold == 40000
    p.add_tower_or_building(t2,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t3,1,1)
    p.towers.append(p.movingObject)
    p.add_tower_or_building(t4,1,1)
    p.towers.append(p.movingObject)
    p.gold -= p.movingObject.buyCost
    assert p.gold<40000
    r=p.gold
    p.gold -= p.towers[0].upgradeCost
    p.towers[0].upgrade()
    assert p.gold<r


def test_damaging_castle():
    p = Castle('Blue','player2')
    p2 = Castle('Red','player1')
    assert p.health==1000
    p.get_hit(10)
    assert p.health<1000
    assert p2.health==1000
    p2.get_hit(10)
    assert p2.health<1000




