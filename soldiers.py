
from audioop import cross
import math
import time
import pygame
from tracemalloc import start
from typing import final
from xmlrpc.client import Boolean
from gameVars import *
from gameBoard import Board, GameCell
from time import sleep
from math import floor


# from castle import Castle


class Soldier:
    def __init__(self, name, x, y, owner, enemy, img):
        self.changed = False
        self.name = name
        self.owner = owner
        self.enemy = enemy
        if x != 0:
            self.x = x
            self.y = y
        elif owner.name == 'player2':
            spawn = [(25, 27), (27, 25)]
            index = random.randint(0, 1)
            self.x, self.y = spawn[index][0] * GRID_WIDTH, spawn[index][1] * GRID_HEIGHT

            # self.x = (x - 1) * GRID_WIDTH
            # self.y = y * GRID_HEIGHT
        else:
            spawn = [(4, 2), (2, 4)]
            index = random.randint(0, 1)
            self.x, self.y = spawn[index]
            # self.x = (x + 2)
            # self.y = y

        # self.health = 100
        self.animationCnt = 0
        self.img = pg.transform.smoothscale(img, (SOLDIER_HEIGHT, SOLDIER_WIDTH))
        self.window = GAME_WINDOW

        self.start = (self.x, self.y)
        self.hasTarget = False
        # self.path = self.astar(Board.gameMatrix, self.start, self.end)
        self.pos = 0
        self.imgs = []
        self.crossRoad = -1
        self.reachedOnce = True
        self.target = None

    def draw(self):
        if self.type == 'Soldier3':
            self.move_strong()
            Board.draw_soldier_movement(self.img, self.x, self.y)
            # self.draw_health(self.window)
        elif self.type == 'Soldier2' and (len(self.enemy.towers) != 0 or len(self.enemy.goldMines) != 0) and not self.changed:
            self.move_strong("towers")
            Board.draw_soldier_movement(self.img, self.x, self.y)

        elif self.type == 'Soldier2' and (len(self.enemy.towers) != 0 or len(self.enemy.goldMines) != 0) and self.changed :
            self.move_strong("towers", True)
            Board.draw_soldier_movement(self.img, self.x, self.y)            
        
        else:
            self.changed = True
            Board.draw_img_on_grid(self.img, self.x, self.y)
            self.move()
        self.draw_health(self.window)

    def load_imgs(self, imgs):
        if self.owner.color == 'Red':
            imgs = [pg.transform.flip(img, True, False) for img in imgs]
        self.imgs = [pg.transform.smoothscale(img, (SOLDIER_HEIGHT, SOLDIER_WIDTH)) for img in imgs]

    def draw_health(self, surf):
        length = self.max_health
        move_by = round(length / self.max_health)
        health_bar = move_by * self.health
        if self.type == 'Soldier3' or (
                self.type == 'Soldier2' and (len(self.enemy.towers) != 0 or len(self.enemy.goldMines) != 0)):
            pygame.draw.rect(surf, RED, (self.y - HEALTH_POSITIONING, self.x - GRID_HEIGHT / 2, length, 10), 0)
            if self.health > 50:
                pygame.draw.rect(surf, GREEN, (self.y - HEALTH_POSITIONING, self.x - GRID_HEIGHT / 2, health_bar, 10),
                                 0)
            else:
                pygame.draw.rect(surf, YELLOW, (self.y - HEALTH_POSITIONING, self.x - GRID_HEIGHT / 2, health_bar, 10),
                                 0)
        else:
            pygame.draw.rect(surf, RED, (
            (self.y * GRID_WIDTH) - HEALTH_POSITIONING, (self.x * GRID_HEIGHT) - GRID_HEIGHT / 2, length, 10), 0)
            if self.health > 50:
                pygame.draw.rect(surf, GREEN, (
                (self.y * GRID_WIDTH) - HEALTH_POSITIONING, (self.x * GRID_HEIGHT) - GRID_HEIGHT / 2, health_bar, 10),
                                 0)
            else:
                pygame.draw.rect(surf, YELLOW, (
                (self.y * GRID_WIDTH) - HEALTH_POSITIONING, (self.x * GRID_HEIGHT) - GRID_HEIGHT / 2, health_bar, 10),
                                 0)

    def alive(self):
        return self.health >= 0

    def die(self):
        self.owner.soldier_killed(self.name)

    def move(self):
        neighbours = [(1, 0), (0, 1), (0, -1), (-1, 0)]
        directions = [[False], [False], [False], [False]]
        for (x, (i, j)) in enumerate(neighbours):
            if self.x + i >= 0 and self.x + i < GRID_WIDTH and self.y + j >= 0 and self.y + j < GRID_HEIGHT:
                if i == 1 or j == 1:
                    x_val, y_val = math.floor(self.x + i), math.floor(self.y + j)
                else:
                    x_val, y_val =  math.ceil(self.x + i), math.ceil(self.y + j)
    
                if Board.gameMatrix[x_val][y_val] == 1:
                    directions[x] = [True, x]

        if self.owner.color == "Blue":
            possibile_routes = [directions[0], directions[1]]
        else:
            possibile_routes = [directions[2], directions[3]]

        if possibile_routes[0][0] == True and possibile_routes[1][0] == True:
            index = random.randint(0, 1)
            if self.crossRoad != -1:
                self.crossRoad = index

            self.x += neighbours[possibile_routes[self.crossRoad][1]][0] / 10
            self.y += neighbours[possibile_routes[self.crossRoad][1]][1] / 10
        elif possibile_routes[0][0] == True:
            self.x += neighbours[possibile_routes[0][1]][0] / 10
            self.y += neighbours[possibile_routes[0][1]][1] / 10
            self.crossRoad = -1
        elif possibile_routes[1][0] == True:
            self.x += neighbours[possibile_routes[1][1]][0] / 10
            self.y += neighbours[possibile_routes[1][1]][1] / 10
            self.crossRoad = -1
        else:
            if self.reachedOnce:
                if self.type == 'Soldier2':
                    self.load_imgs(ANIMATED_SOLDIER2_ATTACK)
                else:
                    self.load_imgs(ANIMATED_SOLDIER1_ATTACK)
                self.reachedOnce = False

            if self.alive():
                self.hit()

            self.slowly_die()

        if self.animationCnt < len(self.imgs) - 1:
            self.animationCnt += 1
            self.img = self.imgs[self.animationCnt]
        else:
            self.animationCnt = 0

    def move_strong(self, type="", changed = False):
        if type == "towers":
            if self.owner.color == "Blue" and not self.hasTarget:
                enemyX = 1000
                enemyY = 1000
                for tower in self.enemy.towers:
                    if math.sqrt(((self.x - tower.x) ** 2) + ((self.y - tower.y) ** 2)) <= math.sqrt(
                            (self.x - enemyX) ** 2) + (((self.y - enemyY) ** 2)):
                        enemyX = tower.x
                        enemyY = tower.y
                        self.target = tower
                        self.hasTarget = True
                for goldmine in self.enemy.goldMines:
                    if math.sqrt(((self.x - goldmine.x) ** 2) + ((self.y - goldmine.y) ** 2)) <= math.sqrt(
                            (self.x - enemyX) ** 2) + (((self.y - enemyY) ** 2)):
                        enemyX = goldmine.x
                        enemyY = goldmine.y
                        self.target = goldmine
                        self.hasTarget = True
                self.end = (enemyY // GRID_WIDTH, enemyX // GRID_WIDTH)

            elif self.owner.color == "Red" and not self.hasTarget:
                if changed:
                    self.x, self.y = self.x * GRID_WIDTH, self.x * GRID_HEIGHT
                enemyX = 1000
                enemyY = 1000
                for tower in self.enemy.towers:
                    if math.sqrt(((self.x - tower.x) ** 2) + ((self.y - tower.y) ** 2)) <= math.sqrt(
                            (self.x - enemyX) ** 2) + (((self.y - enemyY) ** 2)):
                        enemyX = tower.x
                        enemyY = tower.y
                        self.target = tower
                        self.hasTarget = True

                for goldmine in self.enemy.goldMines:
                    if math.sqrt(((self.x - goldmine.x) ** 2) + ((self.y - goldmine.y) ** 2)) <= math.sqrt(
                            (self.x - enemyX) ** 2) + (((self.y - enemyY) ** 2)):
                        enemyX = goldmine.x
                        enemyY = goldmine.y
                        self.target = goldmine
                        self.hasTarget = True
                self.end = (enemyY // GRID_WIDTH, enemyX // GRID_WIDTH)

        if floor(self.x // GRID_WIDTH) == self.end[0] and floor(self.y // GRID_HEIGHT) == self.end[1]:
            if self.reachedOnce:
                if self.type == 'Soldier2':
                    self.load_imgs(ANIMATED_SOLDIER2_ATTACK)
                else:
                    self.load_imgs(ANIMATED_SOLDIER3_ATTACK)
                self.reachedOnce = False

            if type == "towers":
                if self.alive():
                    #self.atcPwer = 3
                    self.attackTower(self.target)

            elif self.alive():
                self.hit()

            self.slowly_die()

        if self.animationCnt < len(self.imgs) - 1:
            self.animationCnt += 1
            self.img = self.imgs[self.animationCnt]
        else:
            self.animationCnt = 0

        if self.x < self.end[0] * GRID_HEIGHT and self.owner.color == "Blue":
            self.x += 2
        elif self.x > self.end[0] * GRID_HEIGHT and self.owner.color == "Red":
            self.x -= 2
        if self.y < self.end[1] * GRID_WIDTH and self.owner.color == "Blue":
            self.y += 2
        elif self.y > self.end[1] * GRID_WIDTH and self.owner.color == "Red":
            self.y -= 2

    def attackTower(self, building):
        building.get_hit(4)
        if building.health <= 0:
            self.enemy.bulidings_killed(building)
            self.die()

    def slowly_die(self):
        # print(self.health)
        self.health -= 0.6
        if self.health <= 0:
            self.die()

    def astar(self, maze, start, end):
        if start == end:
            return []
        # Create start and end node
        start_node = GameCell(None, start)
        start_node.g = start_node.h = start_node.f = 0

        end_node = GameCell(None, end)
        end_node.g = end_node.h = end_node.f = 0

        # Initialize both open and closed list
        open_list = []
        closed_list = []

        # Add the start node
        open_list.append(start_node)

        # Loop until you find the end
        while len(open_list) > 0:

            # Get the current node
            current_node = open_list[0]
            current_index = 0
            for index, item in enumerate(open_list):
                if item.f < current_node.f:
                    current_node = item
                    current_index = index

            # Pop current off open list, add to closed list
            open_list.pop(current_index)
            closed_list.append(current_node)

            # Found the goal
            if current_node == end_node:
                path = []
                current = current_node
                while current is not None:
                    path.append(current.position)
                    current = current.parent
                # path.insert(0,end)
                # print(path)
                finalPath = path[::-1]
                finalPath.append(end)
                return finalPath  # Return reversed path

            # Generate children
            children = []
            for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0)]:  # Adjacent squares

                # Get node position
                node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

                # Make sure within range
                if node_position[0] > (len(maze) - 1) or node_position[0] < 0 or node_position[1] > (
                        len(maze[len(maze) - 1]) - 1) or node_position[1] < 0:
                    continue

                # Make sure walkable terrain
                if maze[node_position[0]][node_position[1]] != 0:
                    continue

                # Create new node
                new_node = GameCell(current_node, node_position)

                # Append
                children.append(new_node)

            # Loop through children
            for child in children:

                # Child is on the closed list
                for closed_child in closed_list:
                    if child == closed_child:
                        continue

                # Create the f, g, and h values
                child.g = current_node.g + 1
                child.h = ((child.position[0] - end_node.position[0]) ** 2) + (
                            (child.position[1] - end_node.position[1]) ** 2)
                child.f = child.g + child.h

                # Child is already in the open list
                for open_node in open_list:
                    if child == open_node and child.g > open_node.g:
                        continue

                # Add the child to the open list
                open_list.append(child)

    def hit(self):
        """Hit the enemy and return if he is dead or not"""
        self.enemy.get_hit(self.atcPwer)

    def takeDmg(self, dmg):
        self.health -= dmg
        if self.health <= 0:
            return True
        return False

class Soldier1(Soldier):
    def __init__(self, name, row, col, owner, enemy):
        self.img = SOLDIER_1
        self.rect = self.img.get_rect()
        self.buyCost = 1000
        self.health = SOLDIER_1_HEALTH
        self.max_health = SOLDIER_1_HEALTH
        self.type = 'Soldier1'
        self.atcPwer = 1
        index = random.randint(0, 1)
        if owner.name == 'player2':
            spawn = [(25, 27), (27, 25)]
            enemySpawn = [(4, 2), (2, 4)]
            self.x, self.y = spawn[index][0], spawn[index][1]
            self.end = enemySpawn[index]

        else:
            spawn = [(4, 2), (2, 4)]
            enemySpawn = [(25, 27), (27, 25)]
            self.x, self.y = spawn[index][0], spawn[index][1]
            self.end = enemySpawn[index]

        super().__init__(name, self.x, self.y, owner, enemy, self.img)



class Soldier2(Soldier):
    def __init__(self, name, row, col, owner, enemy):
        self.img = SOLDIER_2
        self.buyCost = 2000
        self.health = SOLDIER_2_HEALTH
        self.max_health = SOLDIER_2_HEALTH
        self.type = 'Soldier2'
        self.atcPwer = 2
        index = random.randint(0, 1)

        if len(enemy.towers) > 0 or len(enemy.goldMines) > 0:
            if owner.name == 'player2':
                spawn = [(25, 27), (27, 25)]
                self.x, self.y = spawn[index][0] * GRID_WIDTH, spawn[index][1] * GRID_WIDTH
                self.end = (3, 3)
            else:
                spawn = [(0, 2), (0, 2)]
                self.x, self.y = spawn[index][0] * GRID_WIDTH, spawn[index][1] * GRID_WIDTH
                self.end = (26, 26)
            super().__init__(name, self.x, self.y, owner, enemy, self.img)
        else:
            if owner.name == 'player2':
                spawn = [(25, 27), (27, 25)]
                enemySpawn = [(4, 2), (2, 4)]
                self.x, self.y = spawn[index][0], spawn[index][1]
                self.end = enemySpawn[index]

            else:
                spawn = [(4, 2), (2, 4)]
                enemySpawn = [(25, 27), (27, 25)]
                self.x, self.y = spawn[index][0], spawn[index][1]
                self.end = enemySpawn[index]
            super().__init__(name, self.x, self.y, owner, enemy, self.img)



class Soldier3(Soldier):
    def __init__(self, name, row, col, owner, enemy):
        self.img = SOLDIER_3
        self.img = SOLDIER_3
        self.health = SOLDIER_3_HEALTH
        self.max_health = SOLDIER_3_HEALTH
        self.rect = self.img.get_rect()
        self.buyCost = 3000
        self.type = 'Soldier3'
        self.atcPwer = 3
        index = random.randint(0, 1)
        if owner.name == 'player2':
            spawn = [(25, 27), (27, 25)]
            self.x, self.y = spawn[index][0] * GRID_WIDTH, spawn[index][1] * GRID_WIDTH
            self.end = (4, 4)

        else:
            spawn = [(4, 2), (2, 4)]
            self.x, self.y = spawn[index][0] * GRID_WIDTH, spawn[index][1] * GRID_WIDTH
            self.end = (25, 25)

        super().__init__(name, self.x, self.y, owner, enemy, self.img)


