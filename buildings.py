from gameBoard import *
import math
import pygame

class GoldMine:
    def __init__(self, row, col):
        self.health = GOLD_MINE_HEALTH
        self.max_health = GOLD_MINE_HEALTH;
        self.row = row
        self.col = col
        self.name = "goldMine"
        self.x = self.row * GRID_WIDTH
        self.y = self.col * GRID_HEIGHT
        self.buyCost = 1000
        self.gold = 50
        self.width = GRID_WIDTH
        self.height = GRID_HEIGHT
        self.img = GOLD_MINE_IMG
        self.window = GAME_WINDOW

    def get_hit(self, damage):
        self.health -= damage

    def draw(self):
        Board.draw_img_on_grid(self.img, self.row, self.col)
        self.draw_health(self.window)

    def draw_health(self, surf):
        length = 100
        move_by = self.health  /  self.max_health
        health_bar = move_by * length
        
        pygame.draw.rect(surf, RED, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 , length, 10), 0)
        if self.health > (self.max_health/2):
            pygame.draw.rect(surf, GREEN, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 ,health_bar, 10), 0)
        else:
            pygame.draw.rect(surf, YELLOW, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 , health_bar, 10), 0)

    def draw_dragged(self, x, y):
        self.window.blit(self.img, (x-self.width//2, y-self.height//2))
    
    def click(self, X, Y):
        """returns the name of the button we have clicked"""
        if X <= self.col + math.floor(self.width / GRID_WIDTH) - 1 and X >= self.col:
            if Y <= self.row + math.floor(self.height/GRID_HEIGHT) - 1 and Y >= self.row:
                return True
        return False


    def sell(self):
        """sell the gold mine"""
        pass

    def upgrade(self):
        """upgrade the gold mine"""
        self.level += 1
        pass

    def move(self, row, col):
        """move the tower to a given grid"""
        self.row = row
        self.col = col
    
    def incGold(self):
        """inc the gold amount"""
        return self.gold
