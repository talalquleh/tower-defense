import pygame as pg
import random
import os
pg.font.init()
pg.init()
clock = pg.time.Clock()
pg.time.set_timer(pg.USEREVENT, 1000)

# ----------------------------------------------------------
# window and entities vars
# window and entities vars
SCREEN_WIDTH, SCREEN_HEIGHT = 960, 960
GRID_WIDTH, GRID_HEIGHT = 32, 32

# game window
GAME_WINDOW = pg.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

GRIDHOLDER_W, GRIDHOLDER_H =  SCREEN_WIDTH// GRID_WIDTH, SCREEN_HEIGHT // GRID_HEIGHT

CASTLE_SIZE = 5
TOWER_SIZE = 2
TOWER_WIDTH, TOWER_HEIGHT = TOWER_SIZE*GRID_HEIGHT,TOWER_SIZE*GRID_HEIGHT
CASTLE_WIDTH, CASTLE_HEIGHT = GRID_HEIGHT*CASTLE_SIZE, GRID_HEIGHT*CASTLE_SIZE

MENU_ITEM_WIDTH, MENU_ITEM_HEIGHT =  1.5,1.5  

MENU_ITEM_ROW_POS, MENU_ITEM_COL_POS = SCREEN_WIDTH - ((MENU_ITEM_WIDTH+0.3) * GRID_WIDTH) , (SCREEN_HEIGHT // 12)
H_MENU_ITEM_ROW_POS, H_MENU_ITEM_COL_POS = 0,0

# RED_CASTLE_POS = [(19,2), (19,19)]
# BLUE_CASTLE_POS = [(2,2), (2,19)]
RED_CASTLE_POS = [(GRIDHOLDER_H - CASTLE_SIZE ,GRIDHOLDER_W - CASTLE_SIZE ), (GRIDHOLDER_H - CASTLE_SIZE ,GRIDHOLDER_W - CASTLE_SIZE )]
BLUE_CASTLE_POS = [(0,0), (0,0)]
RED_RAND_POS = 0 
BLUE_RAND_POS = random.randint(0, 1)
if BLUE_RAND_POS == 0:
    RED_RAND_POS = 1

# our matrix: 30 column , 23 row
ROWS, COLS = SCREEN_HEIGHT // GRID_HEIGHT, SCREEN_WIDTH // GRID_WIDTH

SOLDIER_SIZE = 1

SOLDIER_WIDTH, SOLDIER_HEIGHT = GRID_WIDTH*SOLDIER_SIZE, GRID_HEIGHT*SOLDIER_SIZE
OBST_SIZE = 1.8
OBST_WIDTH, OBST_HEIGHT = GRID_HEIGHT* OBST_SIZE, GRID_WIDTH * OBST_SIZE
BAR_WIDTH, BAR_HEIGHT = 300, 90
BUTTON_WIDTH, BUTTON_HEIGHT = 100, 40

# colors
RED = (255, 0, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# game vars
VELOCITY = 5
FPS = 60
DIFF_TO_CENTER = 1.2



def loadImage(imgName, width=0, height=0):
    """returns the image scaled or not scaled depending on passed width and height"""
    if width == 0 and height == 0:
        return pg.image.load(os.path.join('Assets',imgName))
    return pg.transform.smoothscale(pg.image.load(os.path.join('Assets',imgName)),(width,height))


# castles images
BLUE_CASTLE_IMG = loadImage('blueCastle.png',CASTLE_WIDTH,CASTLE_HEIGHT)
RED_CASTLE_IMG = loadImage('redCastle.png',CASTLE_WIDTH,CASTLE_HEIGHT)
GAME_BACKGROUND_IMG =loadImage('gameBackground.png')
GOLD_MINE_IMG = loadImage('goldMine.png',TOWER_WIDTH,TOWER_HEIGHT)

# obstacles images
OBST_ONE_IMG =loadImage('obst1.png',OBST_WIDTH,OBST_HEIGHT)
OBST_TWO_IMG = loadImage('obst2.png',OBST_WIDTH,OBST_HEIGHT)

# towers images

TOWER_ONE_IMG = loadImage('tower1.png',TOWER_WIDTH,TOWER_HEIGHT)
TOWER_TWO_IMG = loadImage('tower2.png',TOWER_WIDTH,TOWER_HEIGHT)
TOWER_THREE_IMG = loadImage('tower3.png',TOWER_WIDTH,TOWER_HEIGHT)
RED_TOWER_ONE_IMG = loadImage('redTower1.png',TOWER_WIDTH,TOWER_HEIGHT)
RED_TOWER_TWO_IMG = loadImage('redTower2.png',TOWER_WIDTH,TOWER_HEIGHT)
RED_TOWER_THREE_IMG = loadImage('redTower3.png',TOWER_WIDTH,TOWER_HEIGHT)
BLUE_TOWER_ONE_IMG = loadImage('blueTower1.png',TOWER_WIDTH,TOWER_HEIGHT)
BLUE_TOWER_TWO_IMG = loadImage('blueTower2.png',TOWER_WIDTH,TOWER_HEIGHT)
BLUE_TOWER_THREE_IMG = loadImage('blueTower3.png',TOWER_WIDTH,TOWER_HEIGHT)

TOWER_UPDATE=loadImage('update.png')
TOWER_REMOVE=loadImage('remove.png')
FIRE_BALL=loadImage('pr3.png',10,10)

# players imgs
BLUE_PLAYER_BAR_IMG = loadImage('bluePlayerBar.png',BAR_WIDTH,BAR_HEIGHT)
RED_PLAYER_BAR_IMG = loadImage('redPlayerBar.png',BAR_WIDTH,BAR_HEIGHT)
ROCK_TOWER_IMG = loadImage('rock.png',10,10)

# soldier imgs
SOLDIER_1 =loadImage('sold1/knight1.png')
SOLDIER_2 =loadImage('sold2/knight1.png')
SOLDIER_3 =loadImage('sold3/knight1.png')
ANIMATED_SOLDIER1 = [loadImage('sold1/knight' + str(i)+'.png') for i in range(1, 10)]
ANIMATED_SOLDIER2 = [loadImage('sold2/knight'+ str(i)+'.png') for i in range(1, 10)]
ANIMATED_SOLDIER3 = [loadImage('sold3/knight'+ str(i)+'.png') for i in range(1, 10)]
ANIMATED_SOLDIER1_ATTACK = [loadImage('sold1/kAttack' + str(i)+'.png') for i in range(1, 10)]
ANIMATED_SOLDIER2_ATTACK = [loadImage('sold2/kAttack' + str(i)+'.png') for i in range(1, 10)]
ANIMATED_SOLDIER3_ATTACK = [loadImage('sold3/kAttack' + str(i)+'.png') for i in range(1, 10)]
ANIMATED_SHOT = [loadImage('explo1/ex'+str(i)+'.png',20,20) for i in range(1,7)]
# ----------------------------------------------------------

VERTICAL_MENU = loadImage('VerticalMenu.png',GRID_WIDTH * (MENU_ITEM_WIDTH+0.2),GRID_HEIGHT * MENU_ITEM_HEIGHT * 8) 
HORIZONTAL_MENU = loadImage('HorizontalMenu.png') 
ITEM_BACKGROUND = loadImage('ItemBackground.png') 


SPELL_BACKGROUND = loadImage('spellBackground.png')
SPELL_HEAL = loadImage('healSpell.png')
SPELL_DEATH = loadImage('deathSpell.png')
SPELL_UPGRADE = loadImage('upgradeSpell.png')

# fonts
GAME_FONT = pg.font.SysFont('verdana', 14)
MENU_FONT = pg.font.SysFont('verdana', 10)
TIMER_TURN = 8
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)


SOLDIER_1_HEALTH = 100
SOLDIER_2_HEALTH = 100
SOLDIER_3_HEALTH = 100

HEALTH_POSITIONING = GRID_WIDTH + GRID_WIDTH/4

TOWER_1_HEALTH = 300
TOWER_2_HEALTH = 500
TOWER_3_HEALTH = 400

GOLD_MINE_HEALTH = 500


# SE = SoundEngine()

MUSIC_ON=loadImage('soundOffBtn.png',30,30)
MUSIC_OFF=loadImage('soundOnBtn.png',30,30)
