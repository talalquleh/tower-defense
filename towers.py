from gameBoard import Board
import pygame
import math
from soundEngine import SoundEngine
from menus import *
from gameVars import MENU_FONT


class Tower:
    def __init__(self, row, col):
        self.row = row
        self.col = col
        # self.SE = SoundEngine()
        # self.x = self.row * GRID_WIDTH
        # self.y = self.col * GRID_HEIGHT
        # self.reloadTime = 1
        self.x = self.col * GRID_WIDTH
        self.y = self.row * GRID_HEIGHT
        
        if TOWER_SIZE % 2 == 0:
            self.range = math.floor(self.range_cells / 2) - 1
        else:
            self.range = math.floor(self.range_cells / 2) 
        # self.x =
        # self.y = y
        self.width = TOWER_WIDTH
        self.height = TOWER_HEIGHT
        # self.sellCost
        # self.buyCost
        self.toggle_selected = False
        self.level = 1
        self.Menu = None
        self.img = None
        # TOWER SIZES 8, 10, 12
        # self.range_cells = 12
        # self.range = math.floor(self.range_cells/2) - 1
        self.inRange = False
        self.window = GAME_WINDOW
        self.name = ''
        self.moving = False
        self.shots=[]
        self.enemy_closest=[]
        self.center_radius = math.ceil( (TOWER_SIZE/2 ) - 1)
        # self.col=math.floor(self.x/GRID_HEIGHT)
        # self.row=math.floor(self.y/GRID_WIDTH)
        self.towerMenu=TowerMenu(10,SCREEN_WIDTH-70) 
        self.towerMenu.add_btn(Button(GRID_HEIGHT * 0 + self.towerMenu.x, self.towerMenu.y,
                                            TOWER_UPDATE, 0,
                                            'update'))
        self.towerMenu.add_btn(Button(GRID_HEIGHT*2  + self.towerMenu.x, self.towerMenu.y,
                                            TOWER_REMOVE, 0,
                                            'remove'))



    def draw(self):
        Board.draw_img_on_grid(self.img, self.row, self.col)
        self.draw_level()
        # self.window.blit(
        #     self.img, (self.x-self.width//2, self.y-self.height//2))
        # drawing the range:
        # pg.draw.circle(self.window, (0, 255, 0),
        #                (self.x, self.y), self.range, 4)
        # drawing possible places to build
        # pg.draw.rect()
        self.draw_health(self.window)
        pass

    def draw_health(self, surf):
        length = 100
        move_by = self.health  /  self.max_health
        health_bar = move_by * length
        
        pygame.draw.rect(surf, RED, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 , length, 10), 0)
        if self.health > (self.max_health/2):
            pygame.draw.rect(surf, GREEN, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 ,health_bar, 10), 0)
        else:
            pygame.draw.rect(surf, YELLOW, (self.x - HEALTH_POSITIONING, self.y - GRID_HEIGHT/2 , health_bar, 10), 0)

    def draw_dragged(self, x, y):
        self.window.blit(self.img, (x - self.width // 2, y - self.height // 2))
    
    def click(self, X, Y):
        """returns the name of the button we have clicked"""
        if X <= self.col + math.floor(self.width / GRID_WIDTH) - 1 and X >= self.col:
            if Y <= self.row + math.floor(self.height/GRID_HEIGHT) - 1 and Y >= self.row:
                return True
        return False


    def draw_radius(self):
        if self.toggle_selected:
            # draw range circle
            surface = pygame.Surface((GRID_HEIGHT * self.range_cells, GRID_HEIGHT * self.range_cells), pygame.SRCALPHA,
                                     32)
            # pygame.draw.circle(surface, (128, 128, 128, 100), (self.range, self.range), self.range, 0)
            pygame.draw.rect(surface, (128, 128, 128, 100),
                             [0, 0, GRID_HEIGHT * self.range_cells, GRID_HEIGHT * self.range_cells], 0)
            Board.draw_img_on_grid(surface, self.row - self.range + self.center_radius , self.col - self.range + self.center_radius )

    def sell(self):
        """sell the tower"""
        return self.level * (self.buyCost - 500)

    def upgrade(self):
        """upgrade the tower"""
        if self.level < 3:
            self.level += 1
            self.damage += 10
            self.reloadTime -= 1
            self.upgradeCost*=1.5
        # else:/
        #     # print("It is already max level")
        # # pass

    def changeRange(self, r):
        """updating the tower's firing range"""
        self.range = r

    def move(self, row, col):
        """move the tower to a given grid"""
        self.row = row
        self.col = col
    
    def attack(self, troops):

        self.inRange = False
        self.enemy_closest = []
        for unit in troops:

            if unit.type == 'Soldier3' or  (unit.type == 'Soldier2' and (len(unit.enemy.towers) != 0 or len(unit.enemy.goldMines) != 0)):
                x = unit.x / GRID_HEIGHT
                y = unit.y / GRID_WIDTH

                isIn = x >= self.row - self.range and x <= self.row + self.range + 1 and y >= self.col - self.range and y <= self.col + self.range + 1
            else:
                x = unit.x 
                y = unit.y 
                # print(y,x)
                # print(self.row, self.col)
                isIn = x >= self.row - self.range and x <= self.row + self.range + 1 and y >= self.col - self.range and y <= self.col + self.range + 1

            if isIn:
                self.inRange = True
                # print("enemy is is: ", x, y )
                self.enemy_closest.append(unit)

            if len(self.enemy_closest) > 0 and self.timer % self.reloadTime == 0:
                self.timer = 0
                first_enemy = self.enemy_closest[0]
                if first_enemy.takeDmg(self.damage):
                    pass
                else:
                    self.shoot()
                # print("Soldier was hit :::::::::::::::::::::::::::: ", first_enemy.health)
        
            # SE.play_sounds("laser.wav")

        self.timer += 0.25
    def draw_level(self):
        """
        draws the current level of a tower
        """
        s=pygame.Surface((15, 15))  # the size of your rect
        s.set_alpha(128)                # alpha level
        s.fill((255, 255, 255))           # this fills the entire surface
        self.window.blit(s,(self.x,self.y))
        level=(MENU_FONT.render(
                str(self.level), True, BLACK))
        self.window.blit(level,(self.x,self.y))
    def shoot(self):
        """ draws shots from a tower to closes enemy and removes them once exploaded"""
        self.shots.append(shot(self.col*GRID_WIDTH,self.row*GRID_HEIGHT,self.enemy_closest[0].x,self.enemy_closest[0].y,self.shooting_speed))
        

    def check_in_radius(self,x,y):
        """
       checks if the shoot is in tower range 
        """
        return x >= self.x - self.range and x<= self.x + self.range and y >= self.y - self.range and y<= self.y + self.range




class shot:
    def __init__(self,x,y,targetX,targetY,speed) :
        self.x=x
        self.y=y
        self.targetX=targetX
        self.targetY=targetY
        angle=math.atan2(self.targetY-self.y,self.targetX-self.x) #get angle in radians
        self.speed=speed
        self.dx=math.cos(angle)*self.speed
        self.dy=math.sin(angle)*self.speed
        self.animatedImgs=ANIMATED_SHOT
        self.animationCnt=0
        self.exploaded=False
    def draw(self):
        # pygame.draw.rect(GAME_WINDOW,(0,255,0),self.rect)
        GAME_WINDOW.blit(FIRE_BALL,(self.x,self.y))
        if not self.exploaded:
            self.move()
        else:
            self.animateExplosion()
        
            
    def animateExplosion(self):
        if self.animationCnt<len(self.animatedImgs)-1:
            self.animationCnt += 1
            self.img = self.animatedImgs[self.animationCnt]
        else: 
            self.animationCnt = 0
        GAME_WINDOW.blit(self.animatedImgs[self.animationCnt],(self.x,self.y))
    

    def move(self):
        self.x += (self.dx)
        self.y +=  (self.dy)
        # we need to check if hits target  
        
        self.exploaded=True
        # return





class Tower1(Tower):
    def __init__(self, row, col):
        self.health = TOWER_1_HEALTH
        self.max_health = TOWER_1_HEALTH
        self.img = None
        self.name = 'tower1'
        self.buyCost = 1000
        self.reloadTime = 5

        self.range_cells = TOWER_SIZE + 4

 

        self.timer = 0
        self.shooting_speed=40 
        self.range = math.floor(self.range_cells / 2) 
        self.fire_img = ROCK_TOWER_IMG
        self.damage = 15
        self.upgradeCost=1500
        super().__init__(row, col)

    def get_hit(self, damage):
        self.health -= damage

    def draw(self):
        super().draw_radius()
        super().draw()


class Tower2(Tower):
    def __init__(self, row, col):
        self.health = TOWER_2_HEALTH
        self.max_health = TOWER_2_HEALTH
        self.img = None
        self.buyCost = 2000
        self.name = 'tower2'
        self.reloadTime = 5

        self.range_cells = TOWER_SIZE + 6

        self.timer = 0
        self.shooting_speed=50
        self.fire_img = ROCK_TOWER_IMG
        self.damage = 20
        self.upgradeCost=2500
        super().__init__(row, col)

    def get_hit(self, damage):
        self.health -= damage
    

    def draw(self):
        super().draw_radius()
        super().draw()


class Tower3(Tower):
    def __init__(self, row, col):
        self.health = TOWER_3_HEALTH
        self.max_health = TOWER_3_HEALTH
        self.img = None
        self.name = 'tower3'
        self.buyCost = 3000
        self.reloadTime = 6
        self.range_cells = TOWER_SIZE + 8

        self.timer = 0
        self.shooting_speed=60
        self.fire_img = ROCK_TOWER_IMG
        self.damage = 25
        self.upgradeCost=3500
        super().__init__(row, col)

    def get_hit(self, damage):
        self.health -= damage
    def draw(self):
        super().draw_radius()
        super().draw()



