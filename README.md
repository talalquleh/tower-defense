# CodeDrivers
The game is played on an n × m map. Both players have a castle which is already there (placed
randomly but far apart) at the start of the game.
The goal of the players is to destroy the enemy castle by training soldiers and at the same time
protect their own castle by building towers. By placing the towers right, the enemy soldiers can be
led into a labyrinth and thus forcing them to move between towers that attack them.
The following figure shows an example of the game, where C denotes the castles , T the towers and
O obstacles (e.g. mountains), that can't be passed through.
Building is only possible in the surrounding 2 squares of the players' own buildings, but it is
impossible in the 2 square vicinity of the enemy towers or castle.

