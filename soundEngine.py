from re import X

# from matplotlib.pyplot import xscale
from gameVars import *
# importpg
# import os

class SoundEngine:
    def __init__(self):
        self.music_on = True
        self.width,self.height=30,30
        self.x = SCREEN_WIDTH-self.width
        self.y = 15

    def mute_unmute_music(self):
        self.music_on = not(self.music_on)
        if self.music_on:
           pg.mixer.music.unpause()
        else:
           pg.mixer.music.pause()

    def play_background_music(self, music_name):
       pg.mixer.music.load(os.path.join("Assets/Sounds", music_name))
       pg.mixer.music.play(loops=-1)

    def play_sounds(self, sound_name):
      pg.mixer.Sound(os.path.join("Assets/Sounds", sound_name)).play()
    

    def click(self, X, Y) -> str:
        """returns the name of the button we have clicked"""
        if X <= self.x + self.width and X >= self.x:
            if Y <= self.y + self.height and Y >= self.y:
                return True 

    def draw_button(self):
        if self.music_on:
            GAME_WINDOW.blit(MUSIC_ON,( self.x,self.y))
        else:
            GAME_WINDOW.blit(MUSIC_OFF,(self.x,self.y))
    
