from gameVars import *


class Menu:
    def __init__(self, x, y, width, height, img):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.font = pg.font.SysFont('verdana', 10)
        self.buttons = []
        self.window = GAME_WINDOW
        self.img = img

    def add_btn(self, button):
        """adds a button to the menu"""
        self.buttons.append(button)

    def get_item_cost(self, name):
        """gets cost of item"""
        for btn in self.buttons:
            if btn.name == name:
                return btn.cost
        return -1

    def draw(self):
        """draws vertical menu and its buttons to the screen"""
        
        # GAME_WINDOW.blit(self.img, (self.x , self.y))
        # pg.draw.rect(self.window, BLACK, pg.Rect(
        #     (self.x, self.y), (self.width, self.height)))
        # show all items in the menu with prices
        i = 0
        while i < len(self.buttons):
            self.window.blit(
                self.buttons[i].background_img, (self.buttons[i].x - (GRID_WIDTH*0.4), self.buttons[i].y - (GRID_WIDTH*0.2)))
            self.window.blit(
                self.buttons[i].img, (self.buttons[i].x, self.buttons[i].y))
            item_cost = (self.font.render(
                str(self.buttons[i].price) + " $", True, WHITE))
            self.window.blit(
                item_cost, (self.buttons[i].x, self.buttons[i].y + self.buttons[i].height))
            i += 1


class VerticalMenu(Menu):
    def __init__(self, x, y):
        super().__init__(x, y, GRID_WIDTH*MENU_ITEM_WIDTH, GRID_HEIGHT, VERTICAL_MENU)


    def add_btn(self, button):
        """adds a button to the menu"""
        if len(self.buttons)> 0 :
            self.height +=  button.y - self.buttons[len(self.buttons)-1].y
            # print((button.y - self.buttons[len(self.buttons)-1].y) // 16)
        else: 
            self.height +=  (GRID_HEIGHT*MENU_ITEM_HEIGHT)
        self.buttons.append(button)


class HorizontalMenu(Menu):
    def __init__(self, x, y):
        super().__init__(x, y, GRID_WIDTH, GRID_HEIGHT*MENU_ITEM_HEIGHT, HORIZONTAL_MENU)

    def add_btn(self, button):
        """adds a button to the menu"""
        if len(self.buttons)> 0 :
            self.width +=  button.x - self.buttons[len(self.buttons)-1].x
        else: 
            self.height +=  (GRID_WIDTH*MENU_ITEM_WIDTH)
        self.buttons.append(button)


class TowerMenu(Menu):
    def __init__(self, x, y):
        super().__init__(x, y, GRID_WIDTH*2,GRID_HEIGHT,VERTICAL_MENU)

    def add_btn(self, button):
        """adds a button to the menu"""
        if len(self.buttons)> 0 :
            self.width +=  button.x - self.buttons[len(self.buttons)-1].x 
        else: 
            self.height += (GRID_WIDTH*MENU_ITEM_WIDTH)
        self.buttons.append(button)

class Button:
    def __init__(self, x, y, img, price, name):
        self.x = x
        self.y = y
        self.width = MENU_ITEM_WIDTH * GRID_WIDTH
        self.height = MENU_ITEM_WIDTH * GRID_WIDTH
        if name.find('spell') != -1:
            self.img = pg.transform.smoothscale(img, (self.width - 10, self.height - 10))
            self.background_img = pg.transform.smoothscale(SPELL_BACKGROUND, ((MENU_ITEM_WIDTH+0.7) * GRID_WIDTH, (MENU_ITEM_WIDTH+0.7) * GRID_WIDTH))
        else:
            self.img = pg.transform.smoothscale(img, (self.width, self.height))
            self.background_img = pg.transform.smoothscale(ITEM_BACKGROUND, ((MENU_ITEM_WIDTH+0.7) * GRID_WIDTH, (MENU_ITEM_WIDTH+0.7) * GRID_WIDTH))
            
        self.price = price
        self.name = name

    def click(self, X, Y) -> str:
        """returns the name of the button we have clicked"""
        if X <= self.x + self.width and X >= self.x:
            if Y <= self.y + self.height and Y >= self.y:
                # print(self.name)
                return self.name
